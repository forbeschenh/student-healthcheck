package naplanTests;

import java.io.File;

/**
 * Author: dparakrama Created : Apr 5, 2018
 */
public class FileDeleter {

	public static void fileDeleter(String filename) {
		{
			try {
				File file = new File(filename);
				if (file.delete()) {
					System.out.println(file.getName() + " is deleted!");
				} else {
					System.out.println("Delete operation is failed.");
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

		}

	}
}