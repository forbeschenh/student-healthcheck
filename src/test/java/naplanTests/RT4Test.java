package naplanTests;

import io.github.bonigarcia.wdm.FirefoxDriverManager;

import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import branchingBasicPageObjects.StudentLoginPage;
import branchingBasicPageObjects.TAALoginPage;
import branchingBasicPageObjects.TAASessionCreationPage;

/**
Author: dparakrama
Created : Sep 11, 2018
 */
public class RT4Test {

	static String StudFilepath = "C:\\NAPTESTDATA\\RRSTUD4.xlsx";
	WebDriver driver;
	private TAALoginPage taaloginPage;
	protected TAASessionCreationPage taasessioncreationPage;
	protected StudentLoginPage studentLoginPage;
	static String sessionCode;
	String Xpath = ".//*[@id='content']/div[3]/div[2]/div[1]/div[1]/div[1]/div/h4";

	static String TAFilepath = "C:\\NAPTESTDATA\\RRTA4.xlsx";

	@Parameters({ "TAAappURL", "StudappURL" })
	@Test
	public void roundRobinTest(String TAAappURL, String StudappURL)
			throws InterruptedException, IOException, AWTException {

	//Launch the firefox browser and navugate to TA URL
	FirefoxDriverManager.getInstance().setup();
	driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get(TAAappURL);

	
	// Loop the TA Logins for 350 times
	
	//for (int round =0; round <=300;round++ ) {
		//System.out.println("Strated Round :" + 11+round);
	//Get the TA Username and Password from the excel sheet
	File inputFile = new File(TAFilepath);
	FileInputStream fis = new FileInputStream(inputFile);
	XSSFWorkbook book = new XSSFWorkbook(fis);
	XSSFSheet sheet = book.getSheetAt(0);
	String UserName = sheet.getRow(0).getCell(0).getStringCellValue();
	int x = (int) sheet.getRow(0).getCell(1).getNumericCellValue();
	String Password = String.valueOf(x);
	System.out.println("User Name is " + UserName + "Password is : " + x);

	//Use the Username and Password to login to TA Console 

	taaloginPage = new TAALoginPage(driver);
	taaloginPage.enterUserName(UserName);
	taaloginPage.enterPassword(Password);
	taaloginPage.clickOnSignIn();
	Thread.sleep(4000);
	
	// Create a New TA Session
	taasessioncreationPage = new TAASessionCreationPage(driver);
	taasessioncreationPage.ClickCreateNewTestSession();
	Thread.sleep(4000);
	
	// Get the session code 
	sessionCode = driver.findElement(By.xpath(Xpath)).getText();
	System.out.print("Session Code is  " + sessionCode);
	Thread.sleep(4000);
	
   // Open a second tab and launch the Student Player url

	String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, "t");
	driver.findElement(By.cssSelector("body")).sendKeys(
			selectLinkOpeninNewTab);
	driver.get(StudappURL);
	Thread.sleep(6000);
	
	// Do the Audio check and start the test
	studentLoginPage = new StudentLoginPage(driver);
	studentLoginPage.clickAudioCheck();
	studentLoginPage.clickStartTest();
	Thread.sleep(9000);
	
	
	// Provide the session code
	
	Actions actions = new Actions(driver);
	actions.moveToElement(driver.findElement(By
			.xpath("//*[@id='oneTimeCode']/form/div/div[1]/div[3]/div[1]")));
	actions.sendKeys(sessionCode);
	actions.build().perform();
	Thread.sleep(4000);
	studentLoginPage.clickNext();

	// Get the Student code from excel sheet
	
	File inputFile1 = new File(StudFilepath);
	File outputFile1 = new File(StudFilepath);
	FileInputStream fis1 = new FileInputStream(inputFile1);
	XSSFWorkbook book1 = new XSSFWorkbook(fis1);
	XSSFSheet sheet1 = book1.getSheetAt(0);
	String UserCode = sheet1.getRow(0).getCell(0).getStringCellValue();

	// Provide the Student code and login
	
	actions.moveToElement(driver.findElement(By
			.xpath("//*[@id='oneTimeCode']/form/div/div[1]/div[3]/div[1]")));
	actions.sendKeys(UserCode);
	actions.build().perform();
	Thread.sleep(4000);
	studentLoginPage.clickNext();
	Thread.sleep(2000);
	
	XSSFRow row1 = sheet1.getRow(0);
	fis1.close();
	sheet1.shiftRows(row1.getRowNum() + 1, sheet1.getLastRowNum() + 1, -1);
	OutputStream out1 = new FileOutputStream(outputFile1);
	book1.write(out1);


	//Are you the Student?
	driver.findElement(
			By.cssSelector("button.btn:nth-child(2)"))
				.click();
	Thread.sleep(2000);

	// Go back to TA Console 
	
	String selectTheFirstTab = Keys.chord(Keys.CONTROL, "1");
	driver.findElement(By.cssSelector("body")).sendKeys(selectTheFirstTab);
	driver.switchTo().defaultContent();
	Thread.sleep(12000);
	taasessioncreationPage.clickRefresh();

	
	// Start the session
	Thread.sleep(2000);
	taasessioncreationPage.clickStartSession();
	Thread.sleep(2000);
	
	taasessioncreationPage.clickYestoStartSession();
	Thread.sleep(10000);

	
	// Go To Student Player
	
	String selectSecondTab = Keys.chord(Keys.CONTROL, "2");
	driver.findElement(By.cssSelector("body")).sendKeys(selectSecondTab);
	driver.switchTo().defaultContent();
	Thread.sleep(8000);

	String closetab = Keys.chord(Keys.CONTROL, "w");
	driver.findElement(By.cssSelector("body")).sendKeys(
			closetab);
	

	
	// Loop the 20 students
	for (int y = 1; y <= 19; y++) {
	driver.findElement(By.cssSelector("body")).sendKeys(selectLinkOpeninNewTab);
	driver.get(StudappURL);
	Thread.sleep(6000);
	studentLoginPage.clickAudioCheck();
	studentLoginPage.clickStartTest();
	Thread.sleep(9000);
	
    // Provide the session code
	actions.moveToElement(driver.findElement(By
			.xpath("//*[@id='oneTimeCode']/form/div/div[1]/div[3]/div[1]")));
	actions.sendKeys(sessionCode);
	actions.build().perform();
	Thread.sleep(4000);
	studentLoginPage.clickNext();

	// Get the Student code from excel sheet
	
	UserCode = sheet1.getRow(0).getCell(0).getStringCellValue();
	System.out.println("User Name " + UserCode);
    sheet1.shiftRows(row1.getRowNum() + 1, sheet1.getLastRowNum() + 1,	-1);
	out1 = new FileOutputStream(outputFile1);
	out1.flush();
	out1.close();
	
	
	//Provide the Student Code
	actions.moveToElement(driver.findElement(By
			.xpath("//*[@id='oneTimeCode']/form/div/div[1]/div[3]/div[1]")));
	actions.sendKeys(UserCode);
	actions.build().perform();
	Thread.sleep(4000);
	studentLoginPage.clickNext();
	Thread.sleep(3000);

	//Are you the Student?
	driver.findElement(
			By.cssSelector("button.btn:nth-child(2)"))
				.click();
	Thread.sleep(3000);
	
	
    // Navigate back to TA Console
	String home = Keys.chord(Keys.CONTROL, "1");
	driver.findElement(By.cssSelector("body")).sendKeys(home);
	driver.switchTo().defaultContent();
	Thread.sleep(5000);

	// Navigate to waiting entry
	driver.findElement(By.xpath("//*[contains(text(), 'Waiting entry')]"))
			.click();
	Thread.sleep(7000);
	

	driver.findElement(By.xpath("//*[@id='Grid_1']/div/a[5]")).click();
	Thread.sleep(4000);
	
	// Select the student
	driver.findElement(
			By.xpath("//*[@id='Grid_1']/table/tbody/tr/td[1]/span/label"))
			.click();
	Thread.sleep(4000);
	// Accept the student
	driver.findElement(
			By.xpath("//*[@id='test-attempt-button-panel']/span[3]/a[1]"))
			.click();
	Thread.sleep(4000);
	// Confirm the accept

	driver.findElement(
			By.cssSelector("body > div.modal.fade.in > div > div > div.modal-footer.ng-scope > button.btn.btn-primary.ng-binding"))
			.click();
	Thread.sleep(4000);

	// Navigate back to student palyer
	
	selectSecondTab = Keys.chord(Keys.CONTROL, "2");
	driver.findElement(By.cssSelector("body")).sendKeys(selectSecondTab);
	driver.switchTo().defaultContent();
	Thread.sleep(10000);

	driver.findElement(By.cssSelector("body")).sendKeys(
			closetab);
	
	
	}
	driver.get(TAAappURL);
	Thread.sleep(3000);
	driver.findElement(
				By.cssSelector("li.k-state-default:nth-child(1) > span:nth-child(2)"))
				.click();
		driver.findElement(By.cssSelector("th.k-header:nth-child(1) > span:nth-child(1) > label:nth-child(1)")).click();
		Thread.sleep(4000);

		driver.findElement(
				By.xpath("//*[@id='test-attempt-button-panel']/span[4]/a[3]"))
				.click();

		Thread.sleep(4000);
		driver.findElement(By.cssSelector("button.btn:nth-child(2)")).click();

		Thread.sleep(4000);

		/*driver.findElement(
				By.xpath("//*[@class='btn btn-primary']")).click();
		Thread.sleep(4000);
		
		driver.findElement(
				By.xpath("//*[@id='content']/div[3]/div[1]/div/div/a[5]")).click();
		Thread.sleep(4000);

		driver.findElement(
				By.xpath("//*[@id='ng-app']/body/div[6]/div[3]/div/button[1]")).click();
		Thread.sleep(4000);

		driver.findElement(By.cssSelector("button.btn:nth-child(2)")).click();

		Thread.sleep(4000);*/
}

}

