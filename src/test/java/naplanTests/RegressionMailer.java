package naplanTests;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.Test;

/**
Author: dparakrama
Created : Apr 5, 2018
 */
public class RegressionMailer {

	String file1 = "C:\\ScreenShots\\TA_Login.jpeg";
	String file2 = "C:\\ScreenShots\\Session_Code.jpeg";
	String file3 = "C:\\ScreenShots\\Enter_Session_Code.jpeg";
	String file4 = "C:\\ScreenShots\\Enter_Student_Code.jpeg";
	String file5 = "C:\\ScreenShots\\Student_Logged_In.jpeg";
	String file6 = "C:\\ScreenShots\\Survey_Questions.jpeg";
	String file7 = "C:\\ScreenShots\\Practical_Questions.jpeg";
	String file7_1 = "C:\\ScreenShots\\FinishPractice_Questions.jpeg";
	String file8 = "C:\\ScreenShots\\Strat_Session.jpeg";
	String file8_1 = "C:\\ScreenShots\\Session_Strated.jpeg";
	String file9 = "C:\\ScreenShots\\1Question.jpeg";
	String file10 = "C:\\ScreenShots\\2Question.jpeg";
	String file11 ="C:\\ScreenShots\\3Question.jpeg";
	String file12 ="C:\\ScreenShots\\4Question.jpeg";
	String file13 ="C:\\ScreenShots\\5Question.jpeg";
	String file14 ="C:\\ScreenShots\\6Question.jpeg";
	String file15 ="C:\\ScreenShots\\7Question.jpeg";
	String file16 ="C:\\ScreenShots\\8Question.jpeg";
	String file17 ="C:\\ScreenShots\\9Question.jpeg";
	String file18 ="C:\\ScreenShots\\10Question.jpeg";
	String file19 ="C:\\ScreenShots\\11Question.jpeg";
	String file20 ="C:\\ScreenShots\\12Question.jpeg";
	String file21 ="C:\\ScreenShots\\13Question.jpeg";
	String file22 ="C:\\ScreenShots\\14Question.jpeg";
	String file23 ="C:\\ScreenShots\\15Question.jpeg";
	String file24 ="C:\\ScreenShots\\16Question.jpeg";
	String file25 ="C:\\ScreenShots\\17Question.jpeg";
	String file26 ="C:\\ScreenShots\\18Question.jpeg";
	String file27 ="C:\\ScreenShots\\19Question.jpeg";
	String file28 ="C:\\ScreenShots\\20Question.jpeg";
	String file29 ="C:\\ScreenShots\\21Question.jpeg";
	String file30 ="C:\\ScreenShots\\22Question.jpeg";
	String file31 ="C:\\ScreenShots\\Post_Survey.jpeg";
	String file31_1 ="C:\\ScreenShots\\End_Of_Test.jpeg";
	String file32 ="C:\\ScreenShots\\End_Session.jpeg";
	String file32_1 ="C:\\ScreenShots\\Confirm_To_end.jpeg";
	String file33 ="C:\\ScreenShots\\Logout.jpeg";
	
	
	
	@Test
	public void sendAMailPVTMySCIS() {
		

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"esatestteam@gmail.com", "P3ar$onDP");

					}

				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("esatestteam@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("daupadi.parakrama@esa.edu.au,Sanjeev.Kalta@esa.edu.au,sneha.francis@esa.edu.au,Forbes.Chenh@esa.edu.au"));
			message.setSubject("NAPLAN 2018 | AUTOMATIC MONITORING MAILER");
			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText("PLESE FIND THE ATTACHMENTS");
			Multipart multipart = new MimeMultipart();
			addAttachment(multipart, file1);
			addAttachment(multipart, file2);
			addAttachment(multipart, file3);
			addAttachment(multipart, file4);
			addAttachment(multipart, file5);
			addAttachment(multipart, file6);
			addAttachment(multipart, file7);
			addAttachment(multipart, file7_1);
			addAttachment(multipart, file8);
			addAttachment(multipart, file8_1);
			addAttachment(multipart, file9);
			addAttachment(multipart, file10);
			addAttachment(multipart, file11);
			addAttachment(multipart, file12);
			addAttachment(multipart, file13);
			addAttachment(multipart, file14);
			addAttachment(multipart, file15);
			addAttachment(multipart, file16);
			addAttachment(multipart, file17);
			addAttachment(multipart, file18);
			addAttachment(multipart, file19);
			addAttachment(multipart, file20);
			addAttachment(multipart, file21);
			addAttachment(multipart, file22);
			addAttachment(multipart, file23);
			addAttachment(multipart, file24);
			addAttachment(multipart, file25);
			addAttachment(multipart, file26);
			addAttachment(multipart, file27);
			addAttachment(multipart, file28);
			addAttachment(multipart, file29);
			addAttachment(multipart, file30);
			addAttachment(multipart, file31);
			addAttachment(multipart, file31_1);
			addAttachment(multipart, file32);
			addAttachment(multipart, file32_1);
			addAttachment(multipart, file33);
		    
		    
			message.setContent(multipart);
			Transport.send(message);
			System.out.println("=====Email Sent=====");
			  FileDeleter.fileDeleter(file1);
			  FileDeleter.fileDeleter(file2);
			  FileDeleter.fileDeleter(file3);
			  FileDeleter.fileDeleter(file4);
			  FileDeleter.fileDeleter(file5);
			  FileDeleter.fileDeleter(file6);
			  FileDeleter.fileDeleter(file7);
			  FileDeleter.fileDeleter(file7_1);
			  FileDeleter.fileDeleter(file8);
			  FileDeleter.fileDeleter(file8_1);
			  FileDeleter.fileDeleter(file9);
			  FileDeleter.fileDeleter(file10);
			  FileDeleter.fileDeleter(file11);
			  FileDeleter.fileDeleter(file12);
			  FileDeleter.fileDeleter(file13);
			  FileDeleter.fileDeleter(file14);
			  FileDeleter.fileDeleter(file15);
			  FileDeleter.fileDeleter(file16);
			  FileDeleter.fileDeleter(file17);
			  FileDeleter.fileDeleter(file18);
			  FileDeleter.fileDeleter(file19);
			  FileDeleter.fileDeleter(file20);
			  FileDeleter.fileDeleter(file21);
			  FileDeleter.fileDeleter(file22);
			  FileDeleter.fileDeleter(file23);
			  FileDeleter.fileDeleter(file24);
			  FileDeleter.fileDeleter(file25);
			  FileDeleter.fileDeleter(file26);
			  FileDeleter.fileDeleter(file27);
			  FileDeleter.fileDeleter(file28);
			  FileDeleter.fileDeleter(file29);
			  FileDeleter.fileDeleter(file30);
			  FileDeleter.fileDeleter(file31);
			  FileDeleter.fileDeleter(file31_1);
			  FileDeleter.fileDeleter(file32);
			  FileDeleter.fileDeleter(file32_1);
			  FileDeleter.fileDeleter(file33);
			  
			  
			} catch (MessagingException e) {
			throw new RuntimeException(e);

		}

	}
	
	private static void addAttachment(Multipart multipart, String filename)
			throws MessagingException {
		DataSource source = new FileDataSource(filename);
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);
	}
}
