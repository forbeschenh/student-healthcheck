package naplanTests;

import io.github.bonigarcia.wdm.FirefoxDriverManager;
import java.awt.AWTException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import branchingBasicPageObjects.PreTestPage;
import branchingBasicPageObjects.StudentLoginPage;
import branchingBasicPageObjects.TAALoginPage;
import branchingBasicPageObjects.TAASessionCreationPage;

/**
Author: fchenh
Created : May 09, 2019
 */
public class Naplan2019WritingTest {

	static String StudFilepath = "C:\\NAPTESTDATA\\StudWriting.xlsx";
	WebDriver driver;
	private TAALoginPage taaloginPage;
	protected TAASessionCreationPage taasessioncreationPage;
	protected StudentLoginPage studentLoginPage;
	protected PreTestPage preTestPage;
	static String sessionCode;
	String Xpath = ".//*[@id='content']/div[3]/div[2]/div[1]/div[1]/div[1]/div/h4";

	static String TAFilepath = "C:\\NAPTESTDATA\\TA.xlsx";

	@Parameters({ "TAAappURL", "StudappURL" })
	@Test
	public void takeATest(String TAAappURL, String StudappURL)
			throws InterruptedException, IOException, AWTException {

		FirefoxDriverManager.getInstance().setup();
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get(TAAappURL);

		File inputFile = new File(TAFilepath);
		File outputFile = new File(TAFilepath);
		FileInputStream fis = new FileInputStream(inputFile);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet sheet = book.getSheetAt(0);
		String UserName = sheet.getRow(0).getCell(0).getStringCellValue();
		int x = (int) sheet.getRow(0).getCell(1).getNumericCellValue();
		String Password = String.valueOf(x);
		System.out.println("User Name is " + UserName + "Password is : " + x);

		taaloginPage = new TAALoginPage(driver);
		taaloginPage.enterUserName(UserName);
		taaloginPage.enterPassword(Password);
		takeAScreenShot("TA_Login.jpeg");
		taaloginPage.clickOnSignIn();
		Thread.sleep(4000);
		taasessioncreationPage = new TAASessionCreationPage(driver);
		taasessioncreationPage.ClickCreateNewTestSession();

		sessionCode = driver.findElement(By.xpath(Xpath)).getText();
		System.out.print("Session Code is  " + sessionCode);
		takeAScreenShot("Session_Code.jpeg");
		XSSFRow row = sheet.getRow(0);
		fis.close();
		sheet.shiftRows(row.getRowNum() + 1, sheet.getLastRowNum() + 1, -1);
		OutputStream out = new FileOutputStream(outputFile);
		book.write(out);
		out.flush();
		out.close();

		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, "t");
		driver.findElement(By.cssSelector("body")).sendKeys(
				selectLinkOpeninNewTab);
		driver.get(StudappURL);
		studentLoginPage = new StudentLoginPage(driver);
		studentLoginPage.clickNAPLAN2019();
		Thread.sleep(1500);
		studentLoginPage.clickAudioCheck();
		studentLoginPage.clickStartTest();
		Thread.sleep(9000);
		Actions actions = new Actions(driver);
		actions.moveToElement(driver.findElement(By
				.xpath("//*[@id='oneTimeCode']/form/div/div[1]/div[3]/div[1]")));
		actions.sendKeys(sessionCode);
		actions.build().perform();
		Thread.sleep(4000);
		takeAScreenShot("Enter_Session_Code.jpeg");
		studentLoginPage.clickNext();

		File inputFile1 = new File(StudFilepath);
		File outputFile1 = new File(StudFilepath);
		FileInputStream fis1 = new FileInputStream(inputFile1);
		XSSFWorkbook book1 = new XSSFWorkbook(fis1);
		XSSFSheet sheet1 = book1.getSheetAt(0);
		String UserCode = sheet1.getRow(0).getCell(0).getStringCellValue();

		actions.moveToElement(driver.findElement(By
				.xpath("//*[@id='oneTimeCode']/form/div/div[1]/div[3]/div[1]")));
		actions.sendKeys(UserCode);
		actions.build().perform();
		System.out.println("Student Writing OTC is " + UserCode);
		Thread.sleep(4000);
		takeAScreenShot("Enter_Student_Code.jpeg");
		studentLoginPage.clickNext();
		Thread.sleep(2000);

		XSSFRow row1 = sheet1.getRow(0);
		fis1.close();
		sheet1.shiftRows(row1.getRowNum() + 1, sheet1.getLastRowNum() + 1, -1);
		OutputStream out1 = new FileOutputStream(outputFile1);
		book1.write(out1);
		out1.flush();
		out1.close();

		preTestPage = new PreTestPage(driver);
		preTestPage.answerYes();
		takeAScreenShot("Student_Logged_In.jpeg");
		Thread.sleep(5000);
		/**
		 * 
		
		// Survey And Practical Questions
		//start of practice
		driver.findElement(By.xpath("//*[@class='btn btn-primary ng-binding']"))
				.click();
		Thread.sleep(5000);

		// Survey Question 1
		
		driver.findElement(
				By.xpath("//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/fieldset/div[1]/label"))
				.click();
		Thread.sleep(2000);
		takeAScreenShot("Survey_Questions.jpeg");
		// Finish Survey
		driver.findElement(
				By.xpath("//*[@class='btn btn-primary tp-nav-finish ng-binding ng-scope']"))
				.click();
		Thread.sleep(2000);

		// Practical Question 1 Text

		String QText = driver
				.findElement(
						By.xpath("//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div/div[2]/div/div[1]/p[1]/em"))
				.getText();
		if (QText.contains("Mum uses a")) {

			System.out.println("1ST QUESTION TEXT IS CORRECT");
			Thread.sleep(2000);
			takeAScreenShot("Practical_Questions.jpeg");
		}

		// Click Next Button

		for (int i = 1; i <= 8; i++) {
			actions.moveToElement(driver.findElement(By.xpath("//*[@class='btn btn-primary tp-nav-next ng-binding ng-scope']")));
			actions.click();
			actions.build().perform();
			Thread.sleep(2000);

		}

		// Click Finish Practice Button

		driver.findElement(
				By.xpath("//*[@class='btn btn-primary tp-nav-finish ng-binding ng-scope']"))
				.click();
		takeAScreenShot("FinishPractice_Questions.jpeg");//end of practice
		Thread.sleep(2000); */

		String selectTheFirstTab = Keys.chord(Keys.CONTROL, "1");
		driver.findElement(By.cssSelector("body")).sendKeys(selectTheFirstTab);
		driver.switchTo().defaultContent();
		Thread.sleep(12000);
		taasessioncreationPage.clickRefresh();

		Thread.sleep(2000);
		taasessioncreationPage.clickStartSession();
		Thread.sleep(2000);
		takeAScreenShot("Strat_Session.jpeg");
		taasessioncreationPage.clickYestoStartSession();
		Thread.sleep(10000);
		takeAScreenShot("Session_Started.jpeg");
		String selectSecondTab = Keys.chord(Keys.CONTROL, "2");
		driver.findElement(By.cssSelector("body")).sendKeys(selectSecondTab);
		driver.switchTo().defaultContent();
		
		Thread.sleep(8000);
		
		// Start the Writing Test

		takeAScreenShot("Prompt.jpeg");
		driver.findElement(
				By.cssSelector("body > div.ng-isolate-scope > div.layout-container > div.footer > footer > div.ng-scope > div > div > button"))
				.click();
		Thread.sleep(5000);
		
		// Enter some text in the text editor
		/**
		 * Cannot use this method because the frame ID changes
		 
		driver.switchTo().frame("editor-501_ifr");
		driver.findElement(By.cssSelector("#tinymce")).sendKeys("THIS IS AN ESA QA AUTOMATION TEST");
		driver.switchTo().defaultContent();
		*/
		driver.findElement(By.xpath("//*[@id='ng-app']/body")).sendKeys("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t THIS IS AN ESA QA AUTOMATION TEST");
		Thread.sleep(5000);

		// Finish
		driver.findElement(
				By.cssSelector("body > div.ng-isolate-scope > div.layout-container > div.footer > footer > div.ng-scope > div > div > button"))
				.click();

		Thread.sleep(4000);

		// Are You Sure 

		driver.findElement(By.xpath("//a[contains(text(), 'Yes')]")).click();

		Thread.sleep(5000);
		/**
		 * 
		 
		// Post Survey Finish
		takeAScreenShot("Post_Survey.jpeg");
		driver.findElement(
				By.xpath("//*[@id='ng-app']/body/div[4]/div[3]/footer/div[2]/div/div/button"))
				.click();
        
		// Finish survey
		Thread.sleep(4000);
		takeAScreenShot("End_Of_Test.jpeg");
		*/
		String home = Keys.chord(Keys.CONTROL, "1");
		driver.findElement(By.cssSelector("body")).sendKeys(home);
		driver.switchTo().defaultContent();
		
		Thread.sleep(2000);
		try {
			taasessioncreationPage.clickRefresh();
		} catch (WebDriverException e) {
			System.out.println("Did not click refresh on TA console");
		}
		Thread.sleep(5000);
		
		//click 'ok' to confirm students finished test
		driver.findElement(
				By.xpath(".//*[@class='btn btn-primary']"))
				.click();
		Thread.sleep(4000);
		takeAScreenShot("End_Session.jpeg");
		driver.findElement(
				By.xpath("//*[@id='content']/div[3]/div[1]/div/div/a[5]"))
				.click();
		Thread.sleep(4000);
		// Yes
		driver.findElement(
				By.xpath("//*[@id='ng-app']/body/div[6]/div[3]/div/button[1]"))
				.click();
		Thread.sleep(4000);
		takeAScreenShot("Confirm_To_end.jpeg");
		driver.findElement(By.cssSelector("body > div.modal.fade.in > div > div > div.modal-footer.ng-scope > button.btn.btn-primary.ng-binding"))
				.click();
		
		Thread.sleep(4000);
		takeAScreenShot("Logout.jpeg");
		driver.close();
	}

	
	
	
	
	
	
	
	public void takeAScreenShot (String FileName) throws IOException {
	    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("C:\\WritingTestScreenShots\\"+ FileName));
	
	
}
}