package branchingBasicPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TAASessionCreationPage extends BasicPageObjects {

	String sessionCode;
	String btnCreateNewtestSession = ".//*[@id='content']/form/div[2]/span/button";
	String lblSessionCode = ".//*[@id='content']/div[3]/div[2]/div[1]/div[1]/div[1]/div/h4";
	String btnRefresh = ".//*[@id='Grid_1']/div/a[5]";
	String btnStratSession = ".//*[@id='content']/div[3]/div[2]/div[2]/a[1]";
	String btnADSYes = ".//*[@id='ng-app']/body/div[6]/div[3]/div/button[1]";
	String btnADSOK = ".//*[@id='ng-app']/body/div[6]/div[3]/div/button";
	
	String btnFinalize = ".//*[@id='content']/div[3]/div[1]/div/div/a[5]";
	String btnADSClose = ".//*[@id='ng-app']/body/div[6]/div[3]/div/button[1]";
    String btnClose = ".//*[@id='ng-app']/body/div[6]/div[3]/div/button";
	String btnADSConfirmFinalize = ".//*[@id='ng-app']/body/div[7]/div/div/div[4]/button[2]";
	
	String btnWaitingEntry ="//*[@id='content']/div[3]/div[2]/div[5]/div/div/ul/li[6]/span[2]";
	String btnSelectStudent = "//*[@id='Grid_1']/table/tbody/tr/td[1]/span/label";
	String btnAllowStrat = "//*[@id='test-attempt-button-panel']/span[3]/a[1]";
    String btnConfirm = "//*[@id='ng-app']/body/div[7]/div/div/div[4]/button[2]";
    
	String btnOK = "//*[@id='ng-app']/body/div[6]/div[3]/div/button";

	public TAASessionCreationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void ClickCreateNewTestSession() throws InterruptedException {
		clickButton(btnCreateNewtestSession);

	}

	public void CreateNewTestSession() {
		getString(lblSessionCode, sessionCode);

	}

	public void clickRefresh() throws InterruptedException {
		clickButton(btnRefresh);
	}

	public void clickStartSession() throws InterruptedException {

		clickButton(btnStratSession);

	}

	public void clickOKToCloseSession() throws InterruptedException {

		clickButton(btnADSOK);

	}

	public void clickFinalizeSession() throws InterruptedException {

		clickButton(btnFinalize);

	}

	public void clickCloseSession() throws InterruptedException {

		clickButton(btnADSClose);

	}

	public void clickYestoStartSession() throws InterruptedException {

		clickButton(btnADSYes);

	}

	public void clickConfrimFinalize() throws InterruptedException {

		clickButton(btnADSConfirmFinalize);

	}
	
	 
		public void acceptLaterComer() throws InterruptedException {
			/*clickButton(btnOK);
			Thread.sleep(2000);*/
			clickButton(btnWaitingEntry);
			Thread.sleep(3000);
			clickButton(btnSelectStudent);
			clickButton(btnAllowStrat);
	        clickButton(btnConfirm);
		}

		 
		public void finalizeSession() throws InterruptedException {
			clickButton(btnOK);
			clickButton(btnFinalize);
		    clickButton(btnClose);
		    clickButton(btnADSConfirmFinalize);
		
			
		}

		
		
}
