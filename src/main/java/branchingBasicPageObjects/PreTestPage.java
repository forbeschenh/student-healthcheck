package branchingBasicPageObjects;

import org.openqa.selenium.WebDriver;

public class PreTestPage extends BasicPageObjects {

	String btnNext = ".btn.btn-primary.tp-nav-next.ng-binding.ng-scope";
	String btnFinish = ".btn.btn-primary.tp-nav-finish.ng-binding.ng-scope";
	String btnYesToStart = "button.btn:nth-child(2)";
	String btnPracticeQuestions = ".btn.btn-primary.ng-binding";
	String checkBox1 = "//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div/div[3]/div/div[2]/div/div/div/div/fieldset/div[3]/label";
	String checkBox2 = "//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div/div[3]/div/div[2]/div/div/div/div/fieldset/div[5]/label";
	String btnrefresh = ".//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div/div/div/div";

	public PreTestPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	/****
	 * Choose all the pizzas that are cut in half. Correct Answers : 3, 5
	 * 
	 * @throws InterruptedException
	 */

	public void clickNext() throws InterruptedException {
		clickButtonbyCss(btnNext);

	}

	
	public void answerYes() throws InterruptedException {
		clickButtonbyCss(btnYesToStart);
		Thread.sleep(4000);

	}


}
