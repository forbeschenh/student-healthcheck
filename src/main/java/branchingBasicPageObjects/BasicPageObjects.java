package branchingBasicPageObjects;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;



public class BasicPageObjects {
	private WebDriver driver;

	public BasicPageObjects(WebDriver driver) {
		this.driver = driver;
	}

	/***
	 * Service Method for Clicking links
	 * 
	 * @throws InterruptedException
	 * **/
	public boolean clickLink(String link) throws InterruptedException {
		WebElement Mainlink = driver.findElement(By.xpath(link));
		if (Mainlink.isDisplayed()) {
			System.out.println("Link Text is correct! Link Text is :   "
					+ Mainlink.getText());
			Mainlink.click();
			Thread.sleep(4000);
			return true;
		} else {
			System.out
					.println("Element is not visible or Link Text is incorrect!");
			return false;
		}
	}

	/***
	 * Service Method for Text boxes
	 * 
	 * @throws InterruptedException
	 * **/

	public void enterText(String TextboxXpath, String Text)
			throws InterruptedException {
		WebElement textbox = driver.findElement(By.xpath(TextboxXpath));
		if (textbox.isDisplayed());
		textbox.sendKeys(Text);
		Thread.sleep(4000);
	}

	/***
	 * Service Method for Buttons
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickButton(String ButtonXpath) throws InterruptedException {
		WebElement button = driver.findElement(By.xpath(ButtonXpath));
		if (button.isDisplayed())
			button.click();
		Thread.sleep(4000);
	}

	/***
	 * Service Methods for Buttons
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickButtonbyCss(String ButtonCSS) throws InterruptedException {
		WebElement button = driver.findElement(By.cssSelector(ButtonCSS));
		if (button.isDisplayed())
			button.click();
		Thread.sleep(4000);
	}
	
	public void clickTextList(String TextXpath)
			throws InterruptedException {
		WebElement User = driver.findElement(By.xpath(TextXpath));
		if (User.isDisplayed())
		User.click();
		
	}

	
	public void focus(String element)
			throws InterruptedException {
		WebElement list = driver.findElement(By.xpath(element));
		if (list.isDisplayed())
			list.click();
	}

	/***
	 * Service Method for Buttons
	 * 
	 * @throws InterruptedException
	 * **/


	/***
	 * Service Method for Verify Page Headers
	 * 
	 * @throws InterruptedException
	 * **/
	public boolean verifyPageTitle(String HeaderXapath, String Title)
			throws InterruptedException {
		WebElement PageHeader = driver.findElement(By.xpath(HeaderXapath));
		String HeaderContains = PageHeader.getText();
		if (PageHeader.isDisplayed() && HeaderContains.contains(Title)) {
			System.out.println("Link Text is correct! Link Text is :   "
					+ PageHeader.getText());
			Thread.sleep(4000);
			return true;
		} else {
			System.out
					.println("Page Header Element is not visible or Link Text is incorrect!");
			return false;
		}
	}

	/***
	 * Service Method for DropDowns
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickDropDown(String DropDownXpath, String DropDownValue,
			String Value1, String Value2, String Value3, String Value4)
			throws InterruptedException {
		WebElement dropDown = driver.findElement(By.xpath(DropDownXpath));
		if (dropDown.isDisplayed()) {
			dropDown.click();
			Thread.sleep(1000);

			if (DropDownValue.equals(Value1)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[1]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value2)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[2]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value3)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[3]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value4)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li[4]/div"))
						.click();
			}

			else {

				System.out.println("Element is not displayed");

			}

		}

	}

	/***
	 * Service Method for DropDowns
	 * 
	 * @throws InterruptedException
	 * **/

	public void clickDropDownTypeTwo(String DropDownXpath,
			String DropDownValue, String Value1, String Value2, String Value3,
			String Value4, String Value5) throws InterruptedException {
		WebElement dropDown = driver.findElement(By.xpath(DropDownXpath));
		if (dropDown.isDisplayed()) {
			dropDown.click();
			Thread.sleep(1000);

			if (DropDownValue.equals(Value1)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[1]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value2)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[2]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value3)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[3]/div"))
						.click();
			}

			else if (DropDownValue.equals(Value4)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[4]/div"))
						.click();
			} else if (DropDownValue.equals(Value5)) {
				driver.findElement(
						By.xpath("//*[@id='select2-drop']/ul/li/ul/li[5]/div"))
						.click();
			} else {

				System.out.println("Element is not displayed");

			}

		}

	}

	/***
	 * Service Method to verify the contains of a label (
	 * Ex:- Page Header)
	 * @throws InterruptedException
	 * **/

	public void getContent(String Labelxpath, String Text) {
		WebElement label = driver.findElement(By.xpath(Labelxpath));
		String content = label.getText();
		if (content.contains(Text)) {
			System.out.println("label is correct : " + content);
		} else {
			System.out.println("label is incorrect");
		}

	}

	/***
	 * Service Method for tables and finding values in a table
	 * 
	 * @throws InterruptedException
	 * **/

	public void findTableElement(String tableXpath, String searchStringValue,
			String firstXpathBeforRowNumber, String LastXpathBeforeRowNumber) throws InterruptedException {

		WebElement table = driver.findElement(By.xpath(tableXpath));
		List<WebElement> rows_table = table.findElements(By.tagName("tr"));
		int rows_count = rows_table.size();

		for (int row = 0; row < rows_count; row++) {
			List<WebElement> Columns_row = rows_table.get(row).findElements(
					By.tagName("td"));
			int columns_count = Columns_row.size();

			for (int column = 0; column < columns_count; column++) {
				String celltext = Columns_row.get(column).getText();

				if (celltext.equals(searchStringValue)) {
					driver.findElement(
							By.xpath(firstXpathBeforRowNumber + row
									+ LastXpathBeforeRowNumber)).click();
					Thread.sleep(4000);
				}

			}
		}

	}
	
	/***
	 * Service Method for verifying an element is present
	 * @throws InterruptedException 
	 * 
	 * 
	 * **/

	public boolean isElementPresent(String xpath, String text) throws InterruptedException{
	    try {
	        driver.findElement(By.xpath(xpath));
	        System.out.println("Element Found, Element Text:  "  +driver.findElement(By.xpath(xpath)).getText() );
	        String word =driver.findElement(By.xpath(xpath)).getText();
	        word.equals(text);
	        Thread.sleep(4000);
	        return true;
	    }
	    catch (org.openqa.selenium.NoSuchElementException e){
	        return false;
	    }
	}

	/***
	 * Service Method for getting string content 
	 * @throws InterruptedException 
	 * 
	 * 
	 * **/
	public String getString(String xpath, String content ) {
		content = driver
				.findElement(
						By.xpath(xpath))
				.getText();
		System.out.print("Content is " + content);
		return content;
	}

	public void clickQuestionDropDown(String DropDownCss, String DropDownValue,
			String Value1, String Value2, String Value3, String Value4)
			throws InterruptedException {
		WebElement dropDown = driver.findElement(By.cssSelector(DropDownCss));
		if (dropDown.isDisplayed()) {
			dropDown.click();
			Thread.sleep(1000);

			if (DropDownValue.equals(Value1)) {
				driver.findElement(
						By.xpath(".//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]/a/span[1]"))
						.click();
			}

			else if (DropDownValue.equals(Value2)) {
				driver.findElement(
						By.xpath(".//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]/a/span[2]"))
						.click();
			}

			else if (DropDownValue.equals(Value3)) {
				driver.findElement(
						By.xpath(".//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]/a/span[3]"))
						.click();
			}

			else if (DropDownValue.equals(Value4)) {
				driver.findElement(
						By.xpath(".//*[@id='ng-app']/body/div[3]/div[3]/div[2]/div/div/div[3]/div[2]/div/ul/li[1]/a/span[4]"))
						.click();
			}

			else {

				System.out.println("Element is not displayed");

			}

		}

	}

	
	
	
}
