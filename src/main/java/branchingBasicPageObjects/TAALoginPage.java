package branchingBasicPageObjects;

import org.openqa.selenium.WebDriver;

public class TAALoginPage extends BasicPageObjects {

	String TxtUserName = ".//*[@id='Username']";
	String TxtPassword = ".//*[@id='Password']";
	String BtnLogin = ".btn.btn-primary.button.login";

	public TAALoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void enterUserName(String userName) throws InterruptedException {
		enterText(TxtUserName, userName);
	}

	public void enterPassword(String password) throws InterruptedException {
		enterText(TxtPassword, password);

	}

	public void clickOnSignIn() throws InterruptedException {
		clickButtonbyCss(BtnLogin);
	}

}
