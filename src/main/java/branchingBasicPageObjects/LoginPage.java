package branchingBasicPageObjects;

import org.openqa.selenium.WebDriver;



public class LoginPage extends BasicPageObjects {

	String txtUserName = ".//*[@id='Username']";
	String txtPassword =".//*[@id='Password']";
	String btnLogin = ".btn.btn-primary.button.login";


	
	public LoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}



	public void loginToTheApplication(String userName, String Password) throws InterruptedException {
		enterText(txtUserName, userName);
		enterText(txtPassword, Password);
		clickButtonbyCss(btnLogin);
		Thread.sleep(3000);

}

}