package branchingBasicPageObjects;

import org.openqa.selenium.WebDriver;

public class EnrollmentPage extends BasicPageObjects{

	String btnUserName = ".//*[@id=\"Username\"]";
	String btnPassword = ".//*[@id=\"Password\"]";
	String btnLogin = "//*[@id=\"content\"]/div[3]/div/form/footer/button";
	String txtUser1 = "//*[@id='s2id_userToAdd']/a";
	String txtUser2 = "//*[@id='select2-drop']/div/input";
	String btnAdd = "//*[@class='btn add button button-small btn-primary']";		 
	String ListStudent = "//*[@id='select2-drop']/ul/li[1]/div";
	String EnrollmentTab = "//*[contains(text(), 'User Enrolments')]";


	
	
	
	public EnrollmentPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void Login () throws InterruptedException {
		enterText(btnUserName,"Daupadi");
		enterText(btnPassword,"Password1!");
		clickButton(btnLogin);
			}
	
	public void Opentab () throws InterruptedException {
		clickButton(EnrollmentTab);
		Thread.sleep(4000);
			}
	
	
	public void EnterStudentname (String Studentname) throws InterruptedException {
		clickButton(txtUser1);
		Thread.sleep(2000);
		enterText(txtUser2,Studentname );
			}
	
	
	
	public void selectStudent () throws InterruptedException {
		clickTextList(ListStudent);
		Thread.sleep(2000);
	}
	public void clickButtonAdd () throws InterruptedException {
		clickButton(btnAdd);
		Thread.sleep(2000);
			}
	


	
	

	
	

	
		
	
	
}
